var Discord = require('discord.io');
var auth = require('./auth.json');
var fs = require('fs');

var cmdsFile = require('./commands.js');
var commands = cmdsFile.cmds;
var funcs = cmdsFile.funcs;
var admins = cmdsFile.admins;
console.log(commands);

var bot = new Discord.Client({
   token: auth.token,
   autorun: true
});

console.log("initializing...");

var initialized = false;
bot.commands = commands;

bot.data = null;
bot.lastSave = Date.now();

bot.saveBackup = function(silent = false)
{
    
console.log("saving backup")

    fs.writeFile("./botDataBackup.json", JSON.stringify(bot.data), function(err) {
        if (err) {
            console.log(err);
            bot.sendMessage({
                to: bot.data.lastServer,
                message: "Failed to create a back-up.",
                typing: false
            });
        }
        else
        {
            if(!silent)
            {
                bot.sendMessage({
                    to: bot.data.lastServer,
                    message: "Bot data back-up created.",
                    typing: false
                });
            }
        }
    });
    console.log("backupped")
}

bot.saveData = function(callback, context)
{
    console.log("Saving...");
    try
    {
        if(bot.data != null && bot.data != {} && initialized)
        {
            bot.data = bot.data || {};

            fs.writeFile("./botData.json", JSON.stringify(bot.data), function(err) {
                if (err) {
                    console.log(err);
                }
                
                context = context || this;
                callback && callback.call(context, true);
            });
        }    
    }
    catch(ex)
    {
        console.log("Error while saving!");
        console.log(ex);
        context = context || this;
        callback && callback.call(context, false);
    }
}

console.log("savedata function defined")

bot.on('ready', function (evt) {
    console.log('Connected!');
    console.log('Logged in as: ');
    console.log(bot.username + ' - (' + bot.id + ')');
    
    fs.readFile('./botData.json', function read(err, data) 
    {
        if (err) 
        {
            data = "{}";
        }
        try
        {
            bot.data = JSON.parse(data);
        }
        catch(e)
        {
            if(bot.data == null)
            {
                console.log("Memory broken, will attempt to restore backup.");
            }
        }
        console.log("restoring backup")
        if(bot.data == null || bot.data == {})
        {
            fs.readFile('./botDataBackup.json', function read(err, data) {
                if (err) {
                    data = "{}";
                }
                try
                {
                    bot.data = JSON.parse(data);
                    console.log("Backup restored");

                    if(bot.data.lastServer)
                    {
                        bot.sendMessage({
                            to: bot.data.lastServer,
                            message: "A crash occurred, but I managed to restore a backup of my memory.",
                            typing: false
                        });
                        saveBackup = false;
                    }
                }
                catch(e)
                {
                    if(bot.data == null)
                    {
                        bot.data = {};
                        console.log("Memory backup broken, making new");
                        saveBackup = false;

                        if(bot.data.lastServer)
                        {
                            bot.sendMessage({
                                to: bot.data.lastServer,
                                message: "A crash occurred, and my memory file was lost :(",
                                typing: false
                            });
                        }
                    }
                }
                initialized = true;
            });
        }
        else //successfully loaded
        {
            bot.saveBackup(true);

            initialized = true;
        }

        bot.data = bot.data || {};

        bot.data.servers = bot.data.servers || [];

        if(bot.data.update && bot.data.update != "no")
        {
            bot.sendMessage({
                to: bot.data.update,
                message: "Updated!",
                typing: false
            });

            bot.data.update = "no";
        }
        else
        {
            console.log("no update notifiy to do");
        }

        console.log("Initialized fully");
        
        setInterval(() => {
            funcs.loop(bot);
            console.log("tick")
        }, 500);
    });
});

console.log("onready defined")

bot.on('message', function (user, userID, channelID, message, evt) {
    console.log(message);
    // Our bot needs to know if it will execute a command
    // It will listen for messages that will start with `!`

    if(!initialized)
        return;

    var wasBot = evt.d.author.bot;

    var id = evt.d.guild_id || userID;

    if(bot.data.servers.indexOf(id) < 0)
    {
        bot.data.servers.push(id);
    }

    var group = [userID];

    var words = message.split(' ');
    bot.data[id] = bot.data[id] || {};

    if(Date.now() > bot.lastSave + 3600000) // one hour
    {
        bot.saveBackup(true);
        bot.lastSave = Date.now();
    }

    if(!wasBot)
    {    
        if (message.substring(0, 1) == '&') 
        {
            var args = message.substring(1).split(' ');
            var cmd = args[0];
        
            var info = {user:user, userID:userID, channelID:channelID, message:message, evt:evt, serverId:id};

            bot.data.lastServer = channelID;

            args = args.splice(1);
            for(var i = 0; i < commands.length; i++)
            {
                if(cmd.toLowerCase() == commands[i].cmd)
                {
                    if(commands[i].hidden && admins.indexOf(info.userID) == -1)
                        return;

                    commands[i].execute(bot, info, args);
                    break;
                }
            }
        }

        bot.saveData();
    }
});

console.log("onmessage defined")