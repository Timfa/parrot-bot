var http = require('http');
var fs = require('fs');

exports.funcs = {};
exports.funcs.addMessage = function(bot, serverId, channelId, secondInterval, messageText, onlyOnce = false)
{
    bot.data[serverId] = bot.data[serverId] || {};
    bot.data[serverId].messages = bot.data[serverId].messages || [];

    var idString = "#" + (1000 + bot.data[serverId].messages.length);
    
    idString.replace(/0/g, "A");
    idString.replace(/1/g, "B");
    idString.replace(/2/g, "C");
    idString.replace(/3/g, "D");
    idString.replace(/4/g, "E");
    idString.replace(/5/g, "F");
    idString.replace(/6/g, "G");
    idString.replace(/7/g, "H");
    idString.replace(/8/g, "I");
    idString.replace(/9/g, "J");

    bot.data[serverId].messages.push(
    {
        lastFired: Date.now(),
        interval: secondInterval * 1000,
        message: messageText,
        once: onlyOnce,
        id: idString,
        server: serverId,
        channel: channelId,
        enabled: true
    });
}

exports.funcs.loop = function(bot, serverId)
{
    bot.data.servers.forEach(id => 
    {
        bot.data[id] = bot.data[id] || {};
        bot.data[id].messages = bot.data[id].messages || [];

        for(var i = bot.data[id].messages.length - 1; i >= 0; i--)
        {
            if(bot.data[id].messages[i].lastFired + bot.data[id].messages[i].interval < Date.now() && bot.data[id].messages[i].enabled)
            {
                bot.data[id].messages[i].lastFired = Date.now();
                console.log(bot.data[id].messages[i].id)

                bot.sendMessage({
                    to: bot.data[id].messages[i].channel,
                    message: bot.data[id].messages[i].message,
                    typing: false
                });
                
                console.log(bot.data[id].messages[i].message)

                if(bot.data[id].messages[i].once)
                {
                    bot.data[id].messages[i].enabled = false;
                }

                bot.saveData();
            }
        }
    });
}

exports.funcs.deleteMessage = function(idString)
{
    bot.data.forEach(data => {
        data.messages = data.messages || [];

        for(var i = data.messages.length - 1; i > 0; i--)
        {
            if(data.messages[i].id == idString)
            {
                data.messages[i].enabled = false;

                bot.sendMessage({
                    to: data.messages[i].channelId,
                    message: iDstring + " was cancelled.",
                    typing: false
                });
            }
        }
    });
}

if (!Object.entries) {
    Object.entries = function( obj ){
      var ownProps = Object.keys( obj ),
          i = ownProps.length,
          resArray = new Array(i); // preallocate the Array!
      while (i--)
        resArray[i] = [ownProps[i], obj[ownProps[i]]];
      
      return resArray;
    };
  }

exports.admins = 
[
    "124136556578603009",
    "193093226096361472",
    "176873184484655109"
]

exports.cmds = 
[
    {//
        cmd: "help",
        params: "category",
        category: "main",
        execute: function(bot, info, args)
        {
            var cmds = ["Commands:"];
            var wip = ["Work In Progress (may not work correctly or at all):"]
            
            var categories = ["Categories:"];

            var category = args.join(" ").toLowerCase();

            if(args.length == 0)
            {
                for(var i = 0; i < bot.commands.length; i++)
                {
                    var found = false;

                    for(var c = 0; c < categories.length; c++)
                    {
                        if(categories[c] == "- " + bot.commands[i].category || bot.commands[i].hidden == true || bot.commands[i].category == "wip")
                        {
                            found = true;
                        }
                    }

                    if(!found)
                    {
                        categories.push("- " + bot.commands[i].category);
                    }
                }

                bot.sendMessage({
                    to: info.channelID,
                    message: "Help Categories: ```" + categories.join("\n") /*+ (wip.length > 1? "\n\n" + wip.join("\n") : "")*/ + "```",
                    typing: false
                });
            }
            else
            {
                for(var i = 0; i < bot.commands.length; i++)
                {
                    if(!bot.commands[i].hidden && bot.commands[i].category == category)
                    {
                        var lines = "";

                        for(var a = 0; a < 15 - bot.commands[i].cmd.length; a++)
                            lines += "-";

                        if(!bot.commands[i].wip)
                            cmds.push (">" + bot.commands[i].cmd + " " + lines + " parameters: " + bot.commands[i].params);
                        else
                            wip.push (">" + bot.commands[i].cmd + " " + lines + " parameters: " + bot.commands[i].params);
                    }
                }

                bot.sendMessage({
                    to: info.channelID,
                    message: "Commands: ```" + cmds.join("\n") /*+ (wip.length > 1? "\n\n" + wip.join("\n") : "")*/ + "```",
                    typing: false
                });
            }
        }
    },
    
    {
        cmd: "echo",
        params: "[time], [message]. Example: &echo 50s fifty second message",
        category: "main",
        hidden: true,
        execute:function(bot, info, args)
        {
            if(args[0].match(/[0-9]+[smh]/))
            {
                var unit = args[0][args[0].length - 1];
                var time = args[0].substring(0, args[0].length - 1);

                if(unit == "m")
                    time *= 60;

                if(unit == "h")
                    time *= 60 * 60

                exports.funcs.addMessage(bot, info.serverId, info.channelID, time, args.splice(1, args.length - 1).join(" "), true);

                bot.sendMessage({
                    to: info.channelID,
                    message: "Message will be sent once in " + time + " seconds",
                    typing: false
                });
            }
        }
    },
    {
        cmd: "repeat",
        params: "[time], [message]. Example: &echo 50s fifty second message",
        category: "main",
        hidden: true,
        execute:function(bot, info, args)
        {
            if(args[0].match(/[0-9]+[smh]/))
            {
                var unit = args[0][args[0].length - 1];
                var time = args[0].substring(0, args[0].length - 1);

                if(unit == "m")
                    time *= 60;

                if(unit == "h")
                    time *= 60 * 60

                exports.funcs.addMessage(bot, info.serverId, info.channelID, time, args.splice(1, args.length - 1).join(" "));

                bot.sendMessage({
                    to: info.channelID,
                    message: "Message looping every " + time + " seconds",
                    typing: false
                });
            }
        }
    },
    {
        cmd: "clearchannel",
        params: "none",
        category: "main",
        hidden: true,
        execute:function(bot, info, args)
        {
            var count = 0;
            bot.data.servers.forEach(id => 
            {
                bot.data[id] = bot.data[id] || {};
                bot.data[id].messages = bot.data[id].messages || [];
        
                for(var i = bot.data[id].messages.length - 1; i >= 0; i--)
                {
                    if(bot.data[id].messages[i].channel == info.channelID && bot.data[id].messages[i].enabled)
                    {
                        bot.data[id].messages[i].enabled = false;
                        count++;
                    }
                }
            });
            
            bot.saveData();

            bot.sendMessage({
                to: info.channelID,
                message: "Disabled " + count + " messages!",
                typing: false
            });
        }
    }
]